package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.chat.ReceiveChatEvent;
import de.paxii.clarinet.event.events.player.PlayerSendChatMessageEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.ChatColor;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;

/**
 * Created by Lars on 23.11.2015.
 */
public class ModuleChatAlert extends Module {
  private String lastSentMessage = "";

  public ModuleChatAlert() {
    super("ChatAlert", ModuleCategory.PLAYER);

    this.setEnabled(true);
    this.setDisplayedInGui(false);
    this.setVersion("1.0.1");
    this.setBuildVersion(16005);
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onChat(ReceiveChatEvent receiveChatEvent) {
    String chatMessage = receiveChatEvent.getChatMessage();
    boolean foundName = false;

    if (this.lastSentMessage.length() == 0 || !chatMessage.contains(this.lastSentMessage)) {
      String[] parts = chatMessage.split(" ");

      for (String part : parts) {
        part = ChatColor.stripColor(part)
                .replaceAll("\\?", "")
                .replaceAll("!", "")
                .replaceAll("\\.", "")
                .replaceAll(",", "");

        if (part != null && Wrapper.getPlayer() != null && part.equalsIgnoreCase(Wrapper.getPlayer().getName())) {
          String currentPath = chatMessage.substring(0, chatMessage.indexOf(part));
          String lastColor = ChatColor.getLastColors(currentPath);
          chatMessage = chatMessage.replaceAll(part, ChatColor.RED + part + lastColor);
          foundName = true;
        }
      }
    }

    if (foundName) {
      receiveChatEvent.setChatMessage(chatMessage);
      Wrapper.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
    }
  }

  @EventHandler
  public void sendChat(PlayerSendChatMessageEvent chatMessageEvent) {
    this.lastSentMessage = chatMessageEvent.getChatMessage();
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
